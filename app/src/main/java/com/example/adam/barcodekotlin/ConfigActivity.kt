package com.example.adam.barcodekotlin

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.renderscript.Sampler
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import java.text.SimpleDateFormat
import java.util.*

class ConfigActivity : AppCompatActivity() {

    private var dbHook:DBHelper = DBHelper(this)
    private val ConfigDB:ConfigDB = ConfigDB()
    private var listData = mutableListOf<ConfigItem>()

    var listAdapter = MainListAdapter(this,listData,dbHook)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_config)
        InitToolbar()
        ConfigDB.initTable(dbHook);
        InitList()
    }

    fun createConfigDialog(item:ConfigItem): Dialog {

            val builder = AlertDialog.Builder(this)
            val inflater = layoutInflater.inflate(R.layout.dialog_config,null)

            inflater.findViewById<TextView>(R.id.config_dialog_textView).apply{
                setText(item.param)
            }

            val editTextDialog = inflater.findViewById<EditText>(R.id.config_dialog_editText).apply{
                setText(item.value)
            }

            builder.setView(inflater)
                .setPositiveButton("Zapisz",DialogInterface.OnClickListener{
                        dialog, which ->
                            item.value = editTextDialog.text.toString()
                            ConfigDB.updateItem(dbHook,item)
                            listAdapter.list()
                })
                .setNegativeButton("Anuluj",DialogInterface.OnClickListener{ dialog, which ->
                    dialog.cancel()
                })
            builder.create()
            builder.show()
            return builder.create()


    }


    fun InitToolbar() {
        var toolbarInstance = findViewById(R.id.my_toolbar) as Toolbar?
        setSupportActionBar(toolbarInstance)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()!!.setDisplayShowHomeEnabled(true);
        getSupportActionBar()!!.setTitle("Ustawienia");
    }

    fun InitList() {
        val listView = findViewById<ListView>(R.id.config_listview).apply {
            adapter = listAdapter

        }
        listAdapter.list()

        listView.setOnItemClickListener { parent, view, position, id ->
            val selectedObject = listAdapter.getItem(position)
            createConfigDialog(selectedObject)
        }
    }



    class MainListAdapter(context: Context, var listData:MutableList<ConfigItem>, dbHandler:DBHelper): BaseAdapter() {

        private val mainContext: Context
        private var dbHook:DBHelper = DBHelper(context)
        private val ConfigDB:ConfigDB = ConfigDB()
        init {
            mainContext = context
            dbHook = dbHandler
        }

        override fun getCount(): Int {
            return listData.size
        }

        override fun getItem(position: Int): ConfigItem {
            return listData[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mainContext)
            val rowMain = layoutInflater.inflate(R.layout.listview_config,parent,false)
            rowMain.findViewById<TextView>(R.id.config_header).apply{text =  listData[position].param}
            rowMain.findViewById<TextView>(R.id.config_value).apply{text =  listData[position].value}
            rowMain.findViewById<TextView>(R.id.config_description).apply{text =  listData[position].desc}
            return rowMain
        }

        fun list() {
            listData.clear()
            listData = ConfigDB.getAll(dbHook)
            notifyDataSetChanged()
        }

    }


}
