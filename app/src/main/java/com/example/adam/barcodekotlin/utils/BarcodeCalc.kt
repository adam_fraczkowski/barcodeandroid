package com.example.adam.barcodekotlin

class BarcodeCalc {

    init {

    }

    companion object {

        fun calcEANChecksum(stringNum:String):Int {
            var charSeq = stringNum.toCharArray()
            var nonParitySum:Int = 0
            var paritySum:Int = 0
            var controlVal:Int = 0
            if(charSeq.size==0) return 2
            if(charSeq.size<8 || (charSeq.size>8 && charSeq.size <13)) return 3

            for(i in charSeq.size-1 downTo 0) {

                if(i == charSeq.size - 1)  {
                    controlVal = (charSeq[i].toInt() - 48)
                } else if(i % 2 == 0) {
                    paritySum = paritySum + (charSeq[i].toInt() - 48)
                } else {
                    nonParitySum = nonParitySum + (charSeq[i].toInt() - 48)}
            }
            var checksumValue = 10-((nonParitySum*3 +paritySum)%10)

            if(checksumValue == controlVal) return 1
            else return 0
        }

        fun parseEANChecksumResult(result:Int):String {
            when(result) {
                0->return "Niepoprawna cyfra kontrolna"
                1->return  "Poprawna cyfra kontrolna"
                2->return  "Kod nie został zapisany"
                3->return "Kod za krótki"
            }
            return "Nieokreślony bląd"
        }

    }
}