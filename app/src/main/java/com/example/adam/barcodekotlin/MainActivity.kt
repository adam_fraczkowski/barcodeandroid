package com.example.adam.barcodekotlin

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.widget.*
import android.support.v7.widget.Toolbar
import android.view.*
import android.support.v7.app.AlertDialog
import java.text.SimpleDateFormat
import java.util.*
import com.example.adam.barcodekotlin.BarcodeCalc.Companion.calcEANChecksum
import com.example.adam.barcodekotlin.BarcodeCalc.Companion.parseEANChecksumResult
import org.jetbrains.anko.startActivity
import java.io.File



class MainActivity : AppCompatActivity() {



    val REQUEST_DETAILS_INTENT = 2
    val REQUEST_ALL =8

    private var listData = mutableListOf<BarcodeItem>()
    private var databaseHandler = DBHelper(this@MainActivity)
    private var DB = BarcodeDB()

    private var DBConf = ConfigDB()
    private var Config:MutableList<ConfigItem> = mutableListOf<ConfigItem>()
    private var FileUtils = FileUtils()
    var listAdapter = MainListAdapter(this,databaseHandler,listData,FileUtils)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) 
        DB.initTables(databaseHandler)

        //bind config to object
        Config = DBConf.getAll(databaseHandler)
        FileUtils = FileUtils(Config)

        FileUtils.createDir()
        setContentView(R.layout.activity_main)
        InitToolbar()


        requestPerm()


        val floatingPoint = findViewById<FloatingActionButton>(R.id.fab)
        floatingPoint.setOnClickListener {
            sendDetails(-1)
        }

        val listView = findViewById<ListView>(R.id.main_listview)

        listView.adapter = listAdapter

        listAdapter.list()

        listView.setOnItemClickListener {parent, view, position, id ->
            val selectedObject:BarcodeItem = listAdapter.getItem(position)
            sendDetails(selectedObject.id)
        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            val selectedObject:BarcodeItem = listAdapter.getItem(position)
            deleteDialog(selectedObject.id,listAdapter)
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode==REQUEST_DETAILS_INTENT) {
            listAdapter.list()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater:MenuInflater = menuInflater
        inflater.inflate(R.menu.overflow_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

       return when(item.itemId) {
            R.id.menu_clear -> {
                clearDialog(listAdapter)
                true
            }

            R.id.menu_export-> {
                val ExportedListData = DB.getAllFromDb(databaseHandler)
                val exportFile: File? = FileUtils.exportCsvFile(ExportedListData)
                if(exportFile==null) {
                    Toast.makeText(this,"Nie udało się stworzyć raportu",Toast.LENGTH_LONG);
                } else {
                    val ShareFile:Intent = Intent(Intent.ACTION_SEND);
                    ShareFile.setType("text/csv")
                    ShareFile.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(this,"com.example.android.fileprovider",exportFile))
                    startActivity(Intent.createChooser(ShareFile,"Udostępnij przez..."))
                }
                true
            }

            R.id.menu_config -> {
                runConfigActivity()
                true
            }

            else ->super.onOptionsItemSelected(item)

        }


    }

    fun InitToolbar() {
        var toolbarInstance = findViewById(R.id.my_toolbar) as Toolbar?
        setSupportActionBar(toolbarInstance)
        getSupportActionBar()!!.setDisplayShowHomeEnabled(true);
        getSupportActionBar()!!.setIcon(R.drawable.ic_kapibara_toolbar);
    }

    fun runConfigActivity() {
        var configIntent = Intent(this,ConfigActivity::class.java)
        startActivity(configIntent)
    }

    fun sendDetails(selectedPosId :Int) {
        val formIntent = Intent(this,FormActivity::class.java).apply {
            putExtra("MAIN_LIST_ITEM_ID",selectedPosId )
        }
        startActivityForResult(formIntent,REQUEST_DETAILS_INTENT)
    }


    fun deleteDialog(elementId:Int,adapter: MainListAdapter) {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Usunięcie elementu")
        builder.setMessage("Czy na pewno chcesz usunąć wybrany element ?")
        builder.setPositiveButton("TAK") {dialog,which->
            adapter.remove(elementId)
            Toast.makeText(this,"Usunięto element !",Toast.LENGTH_LONG).show()
        }
        builder.setNegativeButton("NIE") {dialog, which ->

        }
        val dialog:AlertDialog = builder.create()
        dialog.show()
    }

    fun requestPerm() {
        //REQUEST PREMISSIONS
        if (
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED

        ) {

            ActivityCompat.requestPermissions(this, arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),REQUEST_ALL)
        }
    }

    fun clearDialog(adapter: MainListAdapter) {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Wyczyszczenie listy")
        builder.setMessage("Czy na pewno chcesz usunąć wszystkie elementy z listy ?")
        builder.setPositiveButton("TAK") {dialog,which->
            adapter.removeAll()
            Toast.makeText(this,"Usunięto elementy !",Toast.LENGTH_LONG).show()
        }
        builder.setNegativeButton("NIE") {dialog, which ->

        }
        val dialog:AlertDialog = builder.create()
        dialog.show()
    }

    class MainListAdapter(context: Context,dbHandler:DBHelper, var listData:MutableList<BarcodeItem>,var futils:FileUtils): BaseAdapter() {

        private val mainContext: Context
        private var dbHook:DBHelper = DBHelper(context)
        private val BarcodeDB:BarcodeDB = BarcodeDB()
        init {
            mainContext = context
            dbHook = dbHandler
        }

        fun formatTime(timestamp:Long):String {

            return SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Date(timestamp*1000))
        }

        override fun getCount(): Int {
            return listData.size
        }

        override fun getItem(position: Int): BarcodeItem {
            return listData[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mainContext)
            val rowMain = layoutInflater.inflate(R.layout.listview_main,parent,false)
            rowMain.findViewById<TextView>(R.id.main_header).apply{text =  listData.get(position).ident}
            rowMain.findViewById<TextView>(R.id.main_date).apply{text = formatTime(listData.get(position).createdUnixTime)}
            rowMain.findViewById<TextView>(R.id.main_description).apply{
                text = parseEANChecksumResult(calcEANChecksum(listData[position].ident))
            }
            return rowMain
        }

        fun list() {
            listData.clear()
            listData = BarcodeDB.getAllFromDb(dbHook)
            notifyDataSetChanged()
        }

        fun remove(index:Int) {
            futils.removeImageFile(BarcodeDB.getItemFromDb(index,dbHook)!!.imagePath)
            BarcodeDB.deleteItemFromDb(index,dbHook)
            list()
            notifyDataSetChanged()
        }

        fun removeAll() {
            var result = BarcodeDB.deleteAllFromDb(dbHook)
            list()
            notifyDataSetChanged()
        }
    }


}
