package com.example.adam.barcodekotlin

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import io.fotoapparat.Fotoapparat
import io.fotoapparat.log.logcat
import io.fotoapparat.log.loggers
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.selector.back
import kotlinx.android.synthetic.main.activity_camera.*

class CameraActivity : AppCompatActivity() {

    private lateinit var cameraInstance: Fotoapparat

    private var ImageIntent:Intent = Intent()
    private var databaseHandler: DBHelper = DBHelper(this)
    private val ConfigDB:ConfigDB = ConfigDB()
    private var FileUtils = FileUtils()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var configList = ConfigDB.getAll(databaseHandler)
        FileUtils = FileUtils(configList)
        setContentView(R.layout.activity_camera)
        cameraInstance = Fotoapparat(
            context = this,
            view = camera_view,
            scaleType = ScaleType.CenterCrop,
            lensPosition = back(),
            logger = loggers(
                logcat()
            )
        )
        var photoData = intent.extras.get("FILENAME") as String

        findViewById<Button>(R.id.camera_make_button).apply {
            setOnClickListener {
                takePic(photoData)

            }
        }

    }


    override fun onStart() {
        super.onStart()
        cameraInstance.start()
    }

    override fun onStop() {
        super.onStop()
        cameraInstance.stop()
    }

    fun takePic(photoData:String) {

        val imageFile = FileUtils.createImageFile(photoData)
        val PhotoURI =  FileProvider.getUriForFile(
            CameraActivity@this,
            "com.example.android.fileprovider",
            imageFile
        )

        val result = cameraInstance.takePicture()
        result.saveToFile(imageFile).whenAvailable { res->
            ImageIntent.putExtra("photoURI",PhotoURI.toString())
            setResult(Activity.RESULT_OK,ImageIntent)
            finish()
        }

    }
}