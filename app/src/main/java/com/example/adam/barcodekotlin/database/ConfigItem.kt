package com.example.adam.barcodekotlin

data class ConfigItem(
    var id:Int=-1,
    var param   :String    = "",
    var value   :String    = "",
    var desc    :String    = ""
) {
}