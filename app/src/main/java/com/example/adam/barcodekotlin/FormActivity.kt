package com.example.adam.barcodekotlin

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.FileProvider
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.*

import com.example.adam.barcodekotlin.BarcodeCalc.Companion.calcEANChecksum
import com.example.adam.barcodekotlin.BarcodeCalc.Companion.parseEANChecksumResult
import org.jetbrains.anko.*
import java.text.SimpleDateFormat
import java.util.*


class FormActivity : AppCompatActivity() {

    private var databaseHandler: DBHelper = DBHelper(FormActivity@ this)
    private var BarcodeDB = BarcodeDB()
    private var ConfigDB = ConfigDB()
    private var runScanFlag:Boolean = false
    private var FileUtils = FileUtils()

    val REQUEST_IMAGE_CAPTURE = 1
    val SCAN_REQUEST_CODE = 4
    val PICTURE_REQUEST_CODE = 5

    var actualBarcodeItem=BarcodeItem()
    var actualImagePath=""

    override fun onSaveInstanceState(savedInstanceState: Bundle?) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState!!.putBoolean("scanFlag", runScanFlag)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
        //toolbar init
        var configList = ConfigDB.getAll(databaseHandler);
        FileUtils = FileUtils(configList)
        bindCheckbox(configList)
        initToolbar()

        var elementId = intent.extras.get("MAIN_LIST_ITEM_ID") as Int;
        if(savedInstanceState==null) {
            runScanFlag = false
        } else {
            runScanFlag = savedInstanceState!!.getBoolean("scanFlag")
        }

        val dbResult = BarcodeDB.getItemFromDb(elementId,databaseHandler);

        if(dbResult!=null && elementId!=-1)  {
            actualBarcodeItem = dbResult
            bindDataToForm(actualBarcodeItem)
        } else if(elementId==-1 && runScanFlag == false) {
            runScanFlag = true
            startScan()

        }


        var identBarcode = findViewById<EditText>(R.id.form_ident_editview)
        var textViewIdent = findViewById<TextView>(R.id.form_ident_textview)

        identBarcode.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var checksumResult = calcEANChecksum(s.toString())
                textViewIdent.text = parseEANChecksumResult(checksumResult)
            }
        })

      val takePicButton = findViewById<Button>(R.id.form_image_button)
        takePicButton.setOnClickListener {
            findViewById<EditText>(R.id.form_ident_editview).apply{ actualBarcodeItem.ident = text.toString()}
            dispatchTakePictureIntent(actualBarcodeItem.ident)
        }

      findViewById<Button>(R.id.form_savebtn).apply {
          setOnClickListener{
              doAsync {
                  bindDataFromForm(actualBarcodeItem)
                  saveData(actualBarcodeItem)
                  uiThread { toast("Zapisano do bazy danych").show() }
              }
              setResult(Activity.RESULT_OK);
              finish()
          }
      }

     findViewById<Button>(R.id.form_scan_button).apply {
         setOnClickListener {
             startScan()
         }
     }

     findViewById<ImageView>(R.id.form_image_imageview).apply {
         setOnClickListener {
             try {
                 val actualImageUri= FileProvider.getUriForFile(
                     this@FormActivity,
                     "com.example.android.fileprovider",
                     FileUtils.getStorageFileFromImagePath(actualImagePath)
                 )
                 val GalleryIntent = Intent()
                 GalleryIntent.setAction(Intent.ACTION_VIEW)
                 GalleryIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                 GalleryIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                 GalleryIntent.setDataAndType(actualImageUri, "image/jpg")
                 startActivity(GalleryIntent)
             }

             catch (e:Exception) {
                 Toast.makeText(this@FormActivity,"Nie można otworzyć zdjęcia",Toast.LENGTH_LONG)
             }

         }
     }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.form_toolbar_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId) {
            R.id.menu_save -> {
                doAsync {
                    bindDataFromForm(actualBarcodeItem)
                    saveData(actualBarcodeItem)
                    uiThread { toast("Zapisano do bazy danych").show() }
                }
                setResult(Activity.RESULT_OK);
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                actualImagePath = data!!.getStringExtra("photoURI")
                val imageView = findViewById<ImageView>(R.id.form_image_imageview).apply {
                    setImageURI(Uri.parse(actualImagePath))
                }
            } else {
                FileUtils.removeImageFile(actualImagePath)
                Toast.makeText(this, "Nie udało się zrobić zdjęcia", Toast.LENGTH_LONG).show()
            }
        }

        if(requestCode == SCAN_REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                findViewById<EditText>(R.id.form_ident_editview).apply {
                    setText(data!!.getStringExtra("code"))
                }
            } else {
                Toast.makeText(this, "Nie udało się zeskanować kodu", Toast.LENGTH_LONG).show()
            }

            if(actualBarcodeItem.id == -1) {
                findViewById<EditText>(R.id.form_ident_editview).apply{ actualBarcodeItem.ident = text.toString()}
                dispatchTakePictureIntent(actualBarcodeItem.ident)
            }
        }

    }

    fun initToolbar() {
        var toolbarInstance = findViewById(R.id.my_toolbar) as Toolbar?
        setSupportActionBar(toolbarInstance)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()!!.setDisplayShowHomeEnabled(true);
        getSupportActionBar()!!.setTitle("Formularz");
    }

    fun startScan() {
        val scanIntent = Intent(FormActivity@this,ScanActivity::class.java)
        startActivityForResult(scanIntent,SCAN_REQUEST_CODE)
    }

    fun bindDataToForm(dataHolder: BarcodeItem) {
            //get data instances
            findViewById<EditText>(R.id.form_ident_editview).apply { setText(dataHolder.ident) }
            findViewById<TextView>(R.id.form_ident_textview).apply { text = parseEANChecksumResult(calcEANChecksum((dataHolder.ident))) }
            findViewById<ImageView>(R.id.form_image_imageview).apply { setImageURI(Uri.parse(dataHolder.imagePath)) }
            actualImagePath = dataHolder.imagePath;
            findViewById<CheckBox>(R.id.form_checkbox1).apply { setChecked(dataHolder.moduleFeature == 1) }
            findViewById<CheckBox>(R.id.form_checkbox2).apply { setChecked(dataHolder.barcodeFeature == 1) }
            findViewById<CheckBox>(R.id.form_checkbox3).apply { setChecked(dataHolder.printFeature == 1) }
            findViewById<CheckBox>(R.id.form_checkbox4).apply { setChecked(dataHolder.spacesFeature == 1) }
            findViewById<CheckBox>(R.id.form_checkbox5).apply { setChecked(dataHolder.chassisFeature == 1) }
            findViewById<CheckBox>(R.id.form_checkbox6).apply { setChecked(dataHolder.railFeature == 1) }
            findViewById<EditText>(R.id.form_description_edittext).apply { setText(dataHolder.description) }

        }

    fun bindDataFromForm(formItem: BarcodeItem) {
            findViewById<EditText>(R.id.form_ident_editview).apply { formItem.ident = text.toString() }
            formItem.codeValid = calcEANChecksum(formItem.ident)
            formItem.imagePath = actualImagePath
            findViewById<CheckBox>(R.id.form_checkbox1).apply { if (isChecked() == true) formItem.moduleFeature = 1 }
            findViewById<CheckBox>(R.id.form_checkbox2).apply { if (isChecked() == true) formItem.barcodeFeature = 1 }
            findViewById<CheckBox>(R.id.form_checkbox3).apply { if (isChecked() == true) formItem.printFeature = 1 }
            findViewById<CheckBox>(R.id.form_checkbox4).apply { if (isChecked() == true) formItem.spacesFeature = 1 }
            findViewById<CheckBox>(R.id.form_checkbox5).apply { if (isChecked() == true) formItem.chassisFeature = 1 }
            findViewById<CheckBox>(R.id.form_checkbox6).apply { if (isChecked() == true) formItem.railFeature = 1 }
            findViewById<EditText>(R.id.form_description_edittext).apply { formItem.description = text.toString() }
        }

    fun saveData(item: BarcodeItem) {
            item.createdUnixTime = System.currentTimeMillis() / 1000L
            if(item.ident.length==0) {
                item.ident = "00000000"
                item.codeValid = 2
            }

            //rename file if ident has more than 0 length
            if(item.ident.length>0) {
                item.imagePath = FileUtils.renameFile(item.imagePath,item.ident)
            }

            if (item.id == -1) {
                val queryRes = BarcodeDB.InsertItemToDb(item,databaseHandler)
                if(queryRes) Toast.makeText(this,"Zapisano dane w bazie danych !",Toast.LENGTH_LONG).show()
            } else {
                val queryRes = BarcodeDB.updateItem(item,databaseHandler)
                if(queryRes) Toast.makeText(this,"Zaktualizowano dane w bazie danych !",Toast.LENGTH_LONG).show()
            }

        }


        fun dispatchTakePictureIntent(barcodeIdent: String) {

            val cameraIntent = Intent(FormActivity@this,CameraActivity::class.java)
            if(barcodeIdent.length>0) {
                cameraIntent.putExtra("FILENAME",barcodeIdent);
            } else {
                val filename = SimpleDateFormat("ddMMyyyyHHmmss").format(Date(System.currentTimeMillis()));
                cameraIntent.putExtra("FILENAME",filename);
            }

            startActivityForResult(cameraIntent,REQUEST_IMAGE_CAPTURE)
        }

        fun bindCheckbox(items:MutableList<ConfigItem>) {
            for(item in items) {
                when(item.param) {
                    "checkBox1" -> findViewById<CheckBox>(R.id.form_checkbox1).apply { text = item.value }
                    "checkBox2" -> findViewById<CheckBox>(R.id.form_checkbox2).apply { text = item.value }
                    "checkBox3" -> findViewById<CheckBox>(R.id.form_checkbox3).apply { text = item.value }
                    "checkBox4" -> findViewById<CheckBox>(R.id.form_checkbox4).apply { text = item.value }
                    "checkBox5" -> findViewById<CheckBox>(R.id.form_checkbox5).apply { text = item.value }
                    "checkBox6" -> findViewById<CheckBox>(R.id.form_checkbox6).apply { text = item.value }
                    else -> {

                    }

                }
            }
        }
    }


