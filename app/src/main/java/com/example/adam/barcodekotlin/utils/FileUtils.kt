package com.example.adam.barcodekotlin

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.widget.CheckBox
import android.widget.Toast
import java.io.File
import java.io.FileWriter
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class FileUtils(var configList:MutableList<ConfigItem> = mutableListOf<ConfigItem>()) {

        fun createDir() {
            val storageRootDir:String = Environment.getExternalStorageDirectory().toString()
            val barcodeDir:File =  File(storageRootDir + "/kapibara_data")
            if(!barcodeDir.exists()) {
                barcodeDir.mkdirs()
            }

        }

        fun createImageFile(filename:String): File {
            val storageRootDir:String = Environment.getExternalStorageDirectory().toString()
            val barcodeDir:File =  File(storageRootDir + "/kapibara_data")
            barcodeDir.mkdirs()
            val tempFile:File = File.createTempFile(filename+"__",".jpg",barcodeDir)
            return tempFile
        }

        fun renameFile(from:String,to:String):String {
            val storageRootDir:String = Environment.getExternalStorageDirectory().toString()
            val barcodeDir:File =  File(storageRootDir + "/kapibara_data")
            val toFile:File = File.createTempFile(to+"__",".jpg",barcodeDir)
            val RealfromFile =  from.substringAfterLast('/')
            val fromFile = File(storageRootDir + "/kapibara_data/"+RealfromFile)
            fromFile.renameTo(toFile)
            return toFile.toURI().toString()
        }

        fun removeImageFile(filename: String):Boolean {
            val realFilename = filename.substringAfterLast('/')
            val storageRootDir:String = Environment.getExternalStorageDirectory().toString()
            val barcodeFile:File =  File(storageRootDir + "/kapibara_data/"+realFilename)
            if(!barcodeFile.exists()) return false
            else {
                barcodeFile.delete()
                return true
            }
        }

        fun getStorageFileFromImagePath(path:String):File {
            val realFilename = path.substringAfterLast('/')
            val storageRootDir:String = Environment.getExternalStorageDirectory().toString()
            val barcodeFile:File =  File(storageRootDir + "/kapibara_data/"+realFilename)
            return barcodeFile
        }

        fun exportCsvFile(itemlist:List<BarcodeItem>):File? {

            val csvHeader = "id,identyfikator,plik,poprawność,błędy,data,opis"
            var fileWriter : FileWriter? = null

            val storageRootDir:String = Environment.getExternalStorageDirectory().toString()
            val barcodeDir:File =  File(storageRootDir + "/kapibara_data")
            barcodeDir.mkdirs()
            val reportFileName = SimpleDateFormat("dd-MM-yyyy_HH:mm:ss").format(Date(System.currentTimeMillis()))
            val reportFile = File.createTempFile(reportFileName+"__",".csv",barcodeDir)
            try {
                fileWriter = FileWriter(reportFile)
                fileWriter.append(csvHeader)
                fileWriter.append("\n")

                for(item in itemlist) {
                    fileWriter.append(item.id.toString())
                    fileWriter.append(",")
                    fileWriter.append(item.ident)
                    fileWriter.append(",")
                    fileWriter.append(item.imagePath.substringAfterLast("/"))
                    fileWriter.append(",")
                    fileWriter.append(BarcodeCalc.parseEANChecksumResult(item.codeValid))
                    fileWriter.append(",")
                    fileWriter.append(errorParser(item,"#"))
                    fileWriter.append(",")
                    fileWriter.append(SimpleDateFormat("dd-MM-yyyy_HH:mm:ss").format(Date(item.createdUnixTime*1000)))
                    fileWriter.append(",")
                    fileWriter.append(item.description)
                    fileWriter.append("\n")
                }
                fileWriter.close()
                return reportFile
            } catch (e:Exception) {
                return null
            }




        }

        fun errorParser(item:BarcodeItem,separator:String):String {
            var result:String=""
            var checkbox = arrayOf<String>("","","","","","")
            for(item in configList) {
                when(item.param) {
                    "checkBox1" -> checkbox[0] = item.value
                    "checkBox2" -> checkbox[1] = item.value
                    "checkBox3" -> checkbox[2] = item.value
                    "checkBox4" -> checkbox[3] = item.value
                    "checkBox5" -> checkbox[4] = item.value
                    "checkBox6" -> checkbox[5] = item.value
                    else -> {

                    }

                }
            }

            if(item.railFeature==1) result = result + checkbox[5] + separator
            if(item.chassisFeature==1) result = result + checkbox[4] + separator
            if(item.spacesFeature==1) result = result + checkbox[3] + separator
            if(item.printFeature==1) result = result + checkbox[2] + separator
            if(item.barcodeFeature==1) result = result + checkbox[1] + separator
            if(item.moduleFeature==1) result = result + checkbox[0] + separator
            return result
        }





}