package com.example.adam.barcodekotlin

import android.content.ContentValues
import android.database.DatabaseUtils
import java.lang.Exception

class ConfigDB {

    private val SQL_CREATE = "CREATE TABLE IF NOT EXISTS configs (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "param TEXT NOT NULL UNIQUE," +
            "value TEXT," +
            "description TEXT)"


    private val SQL_POPULATE = "INSERT OR IGNORE INTO configs (param,value,description) " +
            "VALUES ('checkBox1','example1','Nazwa pola 1'),('checkBox2','example2','Nazwa pola 2')," +
                    "('checkBox3','example3','Nazwa pola 3'),('checkBox4','example4','Nazwa pola 4')," +
                    "('checkBox5','example5','Nazwa pola 5'),('checkBox6','example6','Nazwa pola 6')"


    private val SQL_DROP = "DROP TABLE IF EXISTS configs"

    fun initTable(dbInstance:DBHelper) {
        var db = dbInstance.writableDatabase
        db.execSQL(SQL_CREATE)
        db.execSQL(SQL_POPULATE)
    }

    //fun insertItem
    fun  insertItem(dbInstance: DBHelper,item:ConfigItem):Boolean {
        var db = dbInstance.writableDatabase
        var values = ContentValues().apply {
            put("param",item.param)
            put("value",item.value)
            put("description",item.desc)
        }
        val insertId = db.insert("configs",null,values)
        return (insertId.compareTo(-1)!=0)

    }

    //fun remove item
    fun removeItem(dbInstance: DBHelper, id:Int):Boolean {
        var db = dbInstance.writableDatabase
        val removeId = db.delete("configs","id=?", arrayOf(id.toString()))
        return removeId==1
    }
    //fun update item
    fun updateItem(dbInstance: DBHelper,item:ConfigItem):Boolean {
        val db = dbInstance.writableDatabase
        val values = ContentValues().apply {
            put("param",item.param)
            put("value",item.value)
            put("description",item.desc)
        }
        val cursor = db.update("configs",values,"id = ?", arrayOf(item.id.toString()))
        return cursor == 1
    }

    //fun get item
    fun getItem(dbInstance: DBHelper,id:Int):ConfigItem {
        val db = dbInstance.readableDatabase
        val item = ConfigItem()
        try {
            val cursor = db.query(
                "configs",
                null,
                "configs.id LIKE ?",
                arrayOf(id.toString()),
                null,
                null,
                null
            )

            cursor.moveToNext()
            cursor.apply {
                item.id = getInt(getColumnIndexOrThrow("id"))
                item.param = getString(getColumnIndexOrThrow("param"))
                item.value = getString(getColumnIndexOrThrow("value"))
                item.desc = getString(getColumnIndexOrThrow("description"))
            }
            cursor.close()
            return item

        } catch (e:Exception) {
            return ConfigItem()
        }

    }

    fun getVal(dbInstance: DBHelper,param:String):String {
        val db = dbInstance.readableDatabase
        var item:String = ""
        try {
            val cursor = db.query(
                "configs",
                null,
                "configs.param LIKE ?",
                arrayOf(param),
                null,
                null,
                null
            )

            cursor.moveToNext()
            cursor.apply {
                item = getString(getColumnIndexOrThrow("value"))
            }
            cursor.close()
            return item

        } catch (e:Exception) {
            return ""
        }

    }


    fun getCount(dbInstance: DBHelper):Int {
        val db = dbInstance.readableDatabase
        try {
            val count = DatabaseUtils.queryNumEntries(db,"configs");
            db.close()
            return count.toInt()
        } catch (e:Exception) {
            return 0
        }
    }

    //get all items
    fun getAll(dbInstance: DBHelper):MutableList<ConfigItem> {
        val db = dbInstance.readableDatabase
        var list:MutableList<ConfigItem> = mutableListOf<ConfigItem>()
        try {
            val cursor = db.query(
                "configs",
                null,
                null,
                null,
                null,
                null,
                null
            )

            while (cursor.moveToNext()) {
                var configItem = ConfigItem()
                cursor.apply {
                    configItem.id = getInt(getColumnIndexOrThrow("id"))
                    configItem.param = getString(getColumnIndexOrThrow("param"))
                    configItem.value = getString(getColumnIndexOrThrow("value"))
                    configItem.desc = getString(getColumnIndexOrThrow("description"))

                }
                list.add(configItem)

            }
            return list

        } catch (e:Exception) {
            return mutableListOf<ConfigItem>()
        }


    }



}