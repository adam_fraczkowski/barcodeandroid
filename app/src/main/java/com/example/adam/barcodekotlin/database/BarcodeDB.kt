package com.example.adam.barcodekotlin

import android.content.ContentValues
import android.database.DatabaseUtils
import java.lang.Exception

class BarcodeDB {

    private val SQL_CREATE = "CREATE TABLE IF NOT EXISTS barcodeItems (" +
            "id INTEGER PRIMARY KEY," +
            "ident TEXT," +
            "imagePath TEXT," +
            "codeValid INTEGER," +
            "printFeature INTEGER," +
            "moduleFeature INTEGER," +
            "railFeature INTEGER," +
            "chassisFeature INTEGER," +
            "spacesFeature INTEGER," +
            "barcodeFeature INTEGER," +
            "unixTime INTEGER," +
            "description TEXT)"

    private val  SQL_DROP = "DROP TABLE IF EXISTS barcodeItems"


    fun initTables(dbInstance:DBHelper) {
        val db = dbInstance.writableDatabase
        db.execSQL(SQL_CREATE)
    }

    fun InsertItemToDb(item:BarcodeItem,dbInstance:DBHelper):Boolean {
        val db = dbInstance.writableDatabase
        val values = ContentValues().apply {
            put("ident",item.ident)
            put("imagePath",item.imagePath)
            put("codeValid",item.codeValid)
            put("printFeature",item.printFeature)
            put("moduleFeature",item.moduleFeature)
            put("railFeature",item.railFeature)
            put("chassisFeature",item.chassisFeature)
            put("spacesFeature",item.spacesFeature)
            put("barcodeFeature",item.barcodeFeature)
            put("unixTime",item.createdUnixTime)
            put("description",item.description)
        }
        val newRowID = db.insert("barcodeItems",null,values)
        return (newRowID.compareTo(-1)!=0)

    }

    fun getItemFromDb(id:Int,dbInstance:DBHelper):BarcodeItem? {
        var barcodeItem:BarcodeItem=BarcodeItem()
        val db = dbInstance.readableDatabase
        try {


            var cursor = db.query(
                "barcodeItems",
                null,
                "barcodeItems.id LIKE ?",
               arrayOf(id.toString()),
                null,
                null,
                null

            )

            cursor.moveToNext()
                barcodeItem.id = cursor.getInt(cursor.getColumnIndexOrThrow("id"))
                barcodeItem.ident = cursor.getString(cursor.getColumnIndexOrThrow("ident"))
                barcodeItem.imagePath = cursor.getString(cursor.getColumnIndexOrThrow("imagePath"))
                barcodeItem.codeValid = cursor.getInt(cursor.getColumnIndexOrThrow("codeValid"))
                barcodeItem.printFeature = cursor.getInt(cursor.getColumnIndexOrThrow("printFeature"))
                barcodeItem.chassisFeature = cursor.getInt(cursor.getColumnIndexOrThrow("chassisFeature"))
                barcodeItem.barcodeFeature = cursor.getInt(cursor.getColumnIndexOrThrow("barcodeFeature"))
                barcodeItem.moduleFeature = cursor.getInt(cursor.getColumnIndexOrThrow("moduleFeature"))
                barcodeItem.railFeature = cursor.getInt(cursor.getColumnIndexOrThrow("railFeature"))
                barcodeItem.spacesFeature = cursor.getInt(cursor.getColumnIndexOrThrow("spacesFeature"))
                barcodeItem.createdUnixTime = cursor.getLong(cursor.getColumnIndexOrThrow("unixTime"))
                barcodeItem.description = cursor.getString(cursor.getColumnIndexOrThrow("description"))

            cursor.close()
            return barcodeItem
        } catch(e: Exception) {
            return null
        }


    }

    fun getAllFromDb(dbInstance:DBHelper):MutableList<BarcodeItem> {
        var barcodeItemList:MutableList<BarcodeItem> = mutableListOf<BarcodeItem>()
        val db = dbInstance.readableDatabase
        try {


            val cursor = db.query(
                "barcodeItems",
                null,
                null,
                null,
                null,
                null,
                "unixTime DESC"

            )
            while (cursor.moveToNext()) {
                var barcodeItem = BarcodeItem()
                barcodeItem.id = cursor.getInt(cursor.getColumnIndexOrThrow("id"))
                barcodeItem.ident = cursor.getString(cursor.getColumnIndexOrThrow("ident"))
                barcodeItem.imagePath = cursor.getString(cursor.getColumnIndexOrThrow("imagePath"))
                barcodeItem.codeValid = cursor.getInt(cursor.getColumnIndexOrThrow("codeValid"))
                barcodeItem.printFeature = cursor.getInt(cursor.getColumnIndexOrThrow("printFeature"))
                barcodeItem.chassisFeature = cursor.getInt(cursor.getColumnIndexOrThrow("chassisFeature"))
                barcodeItem.barcodeFeature = cursor.getInt(cursor.getColumnIndexOrThrow("barcodeFeature"))
                barcodeItem.moduleFeature = cursor.getInt(cursor.getColumnIndexOrThrow("moduleFeature"))
                barcodeItem.railFeature = cursor.getInt(cursor.getColumnIndexOrThrow("railFeature"))
                barcodeItem.spacesFeature = cursor.getInt(cursor.getColumnIndexOrThrow("spacesFeature"))
                barcodeItem.createdUnixTime = cursor.getLong(cursor.getColumnIndexOrThrow("unixTime"))
                barcodeItem.description = cursor.getString(cursor.getColumnIndexOrThrow("description"))
                barcodeItemList.add(barcodeItem)
            }
            cursor.close()
            return barcodeItemList
        } catch(e: Exception) {
            return mutableListOf<BarcodeItem>()
        }
    }

    fun deleteItemFromDb(id:Int,dbInstance:DBHelper):Boolean {
        val db = dbInstance.writableDatabase
        val cursor = db.delete(
            "barcodeItems",
            "id = ?",
            arrayOf(id.toString())
        )
        return (cursor==1)
    }

    fun deleteAllFromDb(dbInstance:DBHelper):Boolean {
        val db = dbInstance.writableDatabase
        val cursor = db.delete(
            "barcodeItems",
            null,
            null
        )
        return (cursor>0)
    }

    fun updateItem(item:BarcodeItem,dbInstance:DBHelper):Boolean {
        val db = dbInstance.writableDatabase
        val values = ContentValues().apply {
            put("ident",item.ident)
            put("imagePath",item.imagePath)
            put("codeValid",item.codeValid)
            put("printFeature",item.printFeature)
            put("moduleFeature",item.moduleFeature)
            put("railFeature",item.railFeature)
            put("chassisFeature",item.chassisFeature)
            put("spacesFeature",item.spacesFeature)
            put("barcodeFeature",item.barcodeFeature)
            put("unixTime",item.createdUnixTime)
            put("description",item.description)
        }
        val result = db.update(
            "barcodeItems",
            values,
            "id = ?",
            arrayOf(item.id.toString())
        )
        return result==1
    }

    fun getCount(dbInstance: DBHelper):Int {
        val db = dbInstance.readableDatabase
        try {
            val count = DatabaseUtils.queryNumEntries(db,"barcodeItems");
            db.close()
            return count.toInt()
        } catch (e:Exception) {
            return 0
        }
    }
}