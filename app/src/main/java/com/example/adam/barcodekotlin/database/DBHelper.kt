package com.example.adam.barcodekotlin

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.lang.Exception

class DBHelper(ctx:Context): SQLiteOpenHelper(ctx,"BarcodeDatabase",null,1) {


    private val SQL_CREATE = "CREATE TABLE IF NOT EXISTS barcodeItems (" +
            "id INTEGER PRIMARY KEY," +
            "ident TEXT," +
            "imagePath TEXT," +
            "codeValid INTEGER," +
            "printFeature INTEGER," +
            "moduleFeature INTEGER," +
            "railFeature INTEGER," +
            "chassisFeature INTEGER," +
            "spacesFeature INTEGER," +
            "barcodeFeature INTEGER," +
            "unixTime INTEGER," +
            "description TEXT)"

    private val  SQL_DROP = "DROP TABLE IF EXISTS barcodeItems"
    init {

    }



    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DROP)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db,oldVersion,newVersion)
    }


}
