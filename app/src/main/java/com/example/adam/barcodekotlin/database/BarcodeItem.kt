package com.example.adam.barcodekotlin



import android.net.Uri
import java.io.Serializable


data class BarcodeItem(var id:Int=-1,
                       var ident:String="",
                       var imagePath: String="",
                       var codeValid:Int=0,
                       var printFeature:Int=0,
                       var moduleFeature:Int=0,
                       var railFeature:Int=0,
                       var chassisFeature:Int=0,
                       var spacesFeature:Int=0,
                       var barcodeFeature:Int=0,
                       var createdUnixTime:Long=0,
                       var description:String=""
                       ):Serializable {

}